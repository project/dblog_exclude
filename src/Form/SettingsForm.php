<?php

/**
 * @file
 * Contains \Drupal\dblog_exclude\SettingsForm
 */

namespace Drupal\dblog_exclude\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use MessengerTrait;
use Drupal\Core\Logger\RfcLogLevel;


/**
 * Configure settings for the dblog_exclude module
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dblog_exclude_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    /*
     * Prefix needs to be the same machine name as the module
     */
    return [
      'dblog_exclude.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dblog_exclude.settings');
    $test = $config->get('severity');

    $log_levels =  RfcLogLevel::getLevels();
    $severity_options = [];
    foreach ($log_levels as $severity) {
      $severity_options[] = (string) $severity;
    }

    /*
     * Retrieve from the current watchdog table a list of unique message types or
     * channels as defined by the core Drupal logger service
     */
    $log_message_types = _dblog_exclude_get_message_types();

    $form['severity'] = [
      '#type' => 'select',
      '#multiple' => true,
      '#options' => $severity_options,
      '#title' => $this->t("Severity"),
      '#default_value' => $config->get('severity'),
      '#description' => $this->t('Exclude recording all errors with the selected severity level(s).'),
    ];

    $form['channels'] = [
      '#type' => 'select',
      '#multiple' => true,
      '#options' => $log_message_types,
      '#title' => $this->t("Exclude these known log message types"),
      '#default_value' => $config->get('channels'),
      '#description' => $this->t('Select one or more message types to exclude. The options are the unique types currently recorded.'),
    ];

    $form['exclude_messages'] = array(
      '#type' => 'textarea',
      '#title' => $this->t("Exclude records containing the following text in the logging error message"),
      '#default_value' => $config->get('exclude_messages'),
      '#description' => $this->t('Multiple text strings need to be entered on a new line.'),
    );



    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {


    $this->config('dblog_exclude.settings')
          ->set('severity', $form_state->getValue('severity'))
          ->save();
    $this->config('dblog_exclude.settings')
      ->set('channels', $form_state->getValue('channels'))
      ->save();
    $this->config('dblog_exclude.settings')
      ->set('exclude_messages', $form_state->getValue('exclude_messages'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
