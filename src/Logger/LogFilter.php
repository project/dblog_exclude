<?php

namespace Drupal\dblog_exclude\Logger;

use Drupal\dblog\Logger\DbLog as BaseDbLog;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Exclude or filter out the logging of events in the watchdog database table.
 */
class logFilter extends BaseDbLog {

  private function strposa($haystack, $needle, $offset=0) {
    if(!is_array($needle)) $needle = array($needle);
    foreach($needle as $query) {
      if(!empty($query)) {
        if (strpos($haystack, $query, $offset) !== FALSE) return TRUE; // stop on first true result
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = array()) {

    $log_event = TRUE;
    $config = \Drupal::config('dblog_exclude.settings');
    $exclude_severities = $config->get('severity');
    $exclude_channels = $config->get('channels');
    $exclude_messages = $config->get('exclude_messages');
    if(!empty($exclude_messages)) {
      //$exclude_messages = preg_split ('/$\R?^/m', $exclude_messages);
      $exclude_messages = explode("\n", str_replace(["\r\n","\n\r","\r"],"\n", $exclude_messages)
      );
    }

    /* Check if any messages matching $level need to be excluded
     * $level is also known as severity - Drupal core defines 8 levels currently
     * ranging from Emergency to Debug
     * https://www.drupal.org/docs/8/api/logging-api/overview
    */
    if(!empty($exclude_severitiesz) && !empty($level)) {
      if(in_array($level,$exclude_severities)) {
        $log_event = FALSE;
      }
    }

    /* Check if any messages match types 'channel' are to be excluded  */
    if(!empty($exclude_channels) && (isset($context['channel']) && !empty($context['channel'])) ) {
      foreach($exclude_channels as $exclusion) {
        if($context['channel'] == $exclusion) {
          $log_event = FALSE;
          break;
        }
      }
    }

    /*
     * Check if any messages contain a string that we want to match on to be excluded
     */
    if(!empty($exclude_messages) && !empty($message) ) {
      if(isset($context['@message'])) {
        $message = strip_tags((string) t($message, ['@message' => $context['@message']]));
      }
      if($this->strposa($message, $exclude_messages) === TRUE) {
          $log_event = FALSE;
        }
    }

    if($log_event) {
      parent::log($level, $message, $context);
    }
  }

}
