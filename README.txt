
CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Installation
 * Configuration

INTRODUCTION
-----------
 * DB LOG Exclude module is used to exclude or filter out log messages from being recorded in the watchdog
   - by type (also known as channel) and level (also known as severity)
   - additionally, you can exclude messages from being recorded that contain a string or matching text
 * Useful to restrict unwanted message types and reduce watchdog size for better performance.
 * Majorly useful in production sites when we want to log only limited messages.
 * You may want to exclude all 'debug' or 'notice' type errors. In this case,
   select both the debug and notice severity options in the module configuration page.

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------
 * Click on Configure link in modules page.
 * Or Go to http://YOURSITE/admin/config/development/dblog_exclude , configure type and
    level and any strings or text that you want to filter or exclude by
 * To restrict by Severity Level, Select from the allowed severity levels.
 * To restrict by Custom Logging, set it up under "Known message types"
   The options that appear under the Known Message Types are the unique message types or channels that have
   been recorded in the site's log so far. With an empty or clean watchdog table, there would be no available
   types to select.
 * If you want to filter or exclude logging of messages containing or matching some text value, then enter the text
   or string values.
